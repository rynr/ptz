package org.rjung.util.ptz;

import org.rjung.util.ptz.event.CamAddedEvent;
import org.rjung.util.ptz.event.CamAddedListener;
import org.rjung.util.ptz.event.CamRemovedEvent;
import org.rjung.util.ptz.event.CamRemovedListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PtzConfiguration {

    private final Set<PtzConfigCam> cams;
    private final Set<CamAddedListener> camAddedListeners;
    private final Set<CamRemovedListener> camRemovedListeners;

    public PtzConfiguration() {
        cams = new HashSet<>();
        camAddedListeners = new HashSet<>();
        camRemovedListeners = new HashSet<>();
    }

    public List<PtzConfigCam> getCams() {
        return new ArrayList<>(this.cams);
    }

    public void removeCam(PtzConfigCam cam) {
        if (cams.contains(cam)) {
            this.cams.remove(cam);
            camRemovedListeners.forEach(camRemovedListener -> camRemovedListener.camRemoved(new CamRemovedEvent(cam)));
        }
    }

    public void addCam(PtzConfigCam cam) {
        if (!cams.contains(cam)) {
            this.cams.add(cam);
            camAddedListeners.forEach(camAddedListener -> camAddedListener.camAdded(new CamAddedEvent(cam)));
        }
    }

    public void addCamAddedListener(CamAddedListener camAddedListener) {
        this.camAddedListeners.add(camAddedListener);
    }

    public void removeCamAddedListener(CamAddedListener camAddedListener) {
        this.camAddedListeners.remove(camAddedListener);
    }

    public int numberOfCams() {
        return cams.size();
    }

}
