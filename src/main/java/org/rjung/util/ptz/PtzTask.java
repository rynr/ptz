package org.rjung.util.ptz;

import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.SecureRandom;
import java.time.Duration;
import java.util.Random;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

class PtzTask extends TimerTask {
    private final Logger LOG = Logger.getLogger("PtzTask");
    private final Random RANDOM = new SecureRandom();

    private final PtzConfiguration configuration;
    private final AtomicInteger counter;
    // http://<host>/cgi-bin/ptzctrl.cgi?ptzcmd&<poscall,posset>&<preset>
    // http://<host>/cgi-bin/ptzctrl.cgi?ptzcmd&<right,left,up,down,ptzstop>&<panspeed>&<tiltspeed>
    // http://<host>/cgi-bin/ptzctrl.cgi?ptzcmd&<zoomin,zoomout,zoomstop>&<zoomspeed>
    // http://<host>/cgi-bin/ptzctrl.cgi?ptzcmd&<focusin,focusout,focusstop>&<focusspeed>
    private final HttpClient httpClient;

    public PtzTask(PtzConfiguration configuration) {
        this.configuration = configuration;
        this.counter = new AtomicInteger();
        this.httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .followRedirects(HttpClient.Redirect.NEVER)
                .connectTimeout(Duration.ofSeconds(20))
                .authenticator(new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("admin", "admin".toCharArray());
                    }
                })
                .build();
    }

    @Override
    public void run() {
        var step = counter.getAndIncrement();
        var camNr = step % numberOfCams();
        var cam = getCam(camNr);
        var camStep = (step - camNr) / numberOfCams();
        var preset = switch (cam.select()) {
            case ASC -> camStep % cam.presets();
            case DESC -> cam.presets() - 1 - (camStep % cam.presets());
            case RND -> RANDOM.nextInt(cam.presets());
        };
        var request = HttpRequest.newBuilder()
                .uri(URI.create("http://" + cam.host() + "/cgi-bin/ptzctrl.cgi?ptzcmd&poscall&" + preset))
                .build();
        try {
            httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            LOG.warning(e.getMessage());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private int numberOfCams() {
        return this.configuration.numberOfCams();
    }

    private PtzConfigCam getCam(int index) {
        return this.configuration.getCams().get(index);
    }

}