package org.rjung.util.ptz.event;

import java.util.EventListener;

public interface CamRemovedListener extends EventListener {
    void camRemoved(CamRemovedEvent camRemoved);
}
