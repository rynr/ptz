package org.rjung.util.ptz.event;

import java.util.EventListener;

public interface CamAddedListener extends EventListener {
    void camAdded(CamAddedEvent camAdded);
}
