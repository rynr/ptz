package org.rjung.util.ptz.event;

import org.rjung.util.ptz.PtzConfigCam;

public record CamRemovedEvent(PtzConfigCam cam) {
}
