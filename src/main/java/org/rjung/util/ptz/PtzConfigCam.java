package org.rjung.util.ptz;

public record PtzConfigCam(String name, String host, int presets, PtzConfigCamSelect select) {
    @Override
    public String toString() {
        return name;
    }
}