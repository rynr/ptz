package org.rjung.util.ptz;

import org.rjung.util.ptz.ui.PtzFrame;

import java.util.Timer;

public class PtzApplication implements Runnable {
    private final Timer timer;
    private final PtzConfiguration configuration;
    private final PtzFrame application;

    public static void main(String[] args) {
        new PtzApplication().run();
    }

    private PtzApplication() {
        configuration = new PtzConfiguration();
        configuration.addCam(new PtzConfigCam("Cam 1 RND", "192.168.1.107", 3, PtzConfigCamSelect.RND));
        configuration.addCam(new PtzConfigCam("Cam 2 RND", "192.168.1.122", 3, PtzConfigCamSelect.RND));
        application = setupGui();
        timer = new Timer();
    }

    @Override
    public void run() {
        timer.scheduleAtFixedRate(new PtzTask(configuration), 0, 3333);
    }

    private PtzFrame setupGui() {
        return new PtzFrame(this.configuration);
    }

}
