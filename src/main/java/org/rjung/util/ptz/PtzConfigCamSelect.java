package org.rjung.util.ptz;

public enum PtzConfigCamSelect {
    ASC, DESC, RND
}
