package org.rjung.util.ptz.ui;

import org.rjung.util.ptz.PtzConfigCam;
import org.rjung.util.ptz.PtzConfigCamSelect;
import org.rjung.util.ptz.PtzConfiguration;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.logging.Logger;

public class PtzFrame extends JFrame implements ActionListener, ChangeListener {
    private final static Logger LOG = Logger.getLogger("PtzFrame");

    private final PtzConfiguration configuration;

    public PtzFrame(PtzConfiguration configuration) {
        super("PTZ");
        this.configuration = configuration;
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(600, 400));
        setIconImage(new ImageIcon("ptz.png").getImage());

        add(new JButton("RUN"), BorderLayout.NORTH);
        JSplitPane splitpane = new
                JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        var cams = new DefaultListModel<PtzConfigCam>();
        var camList = new JList<>(cams);
        this.configuration.getCams().forEach(cam -> {
            LOG.warning("Received new cam " + cam);
            cams.addElement(cam);
        });
        camList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        camList.addListSelectionListener(e -> {
            if(!e.getValueIsAdjusting()) {
                var currentCal = camList.getSelectedValue();
                LOG.warning("Current Cam: " + currentCal);
            }
        });
        camList.setPreferredSize(new Dimension(140, 120));
        add(splitpane, BorderLayout.CENTER);
        var camScroll = new JScrollPane(camList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        splitpane.setLeftComponent(camScroll);
        var camera = new PtzCamera(new PtzConfigCam("Cam Z", "host", 0, PtzConfigCamSelect.RND));
        splitpane.setRightComponent(camera);

        setJMenuBar(buildMenuBar());
        setVisible(true);

        this.configuration.addCamAddedListener(added -> camList.add(new JLabel(added.cam().name())));
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        LOG.info(e.toString());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        LOG.info(e.toString());
        LOG.fine("Received " + e.getActionCommand() + " request");
        if ("EXIT".equals(e.getActionCommand())) {
            System.exit(0);
        }
    }

    private JMenuBar buildMenuBar() {
        var result = new JMenuBar();
        var fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        var exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.setActionCommand("EXIT");
        exitMenuItem.setMnemonic(KeyEvent.VK_X);
        exitMenuItem.addActionListener(this);
        fileMenu.add(exitMenuItem);
        var camMenu = new JMenu("Cam");
        camMenu.setMnemonic(KeyEvent.VK_C);
        result.add(fileMenu);
        var newCamMenuItem = new JMenuItem("New Camera");
        newCamMenuItem.setActionCommand("NEW_CAMERA");
        newCamMenuItem.setMnemonic(KeyEvent.VK_C);
        newCamMenuItem.addActionListener(this);
        camMenu.add(newCamMenuItem);
        result.add(camMenu);
        return result;
    }

}
