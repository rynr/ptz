package org.rjung.util.ptz.ui;

import org.rjung.util.ptz.PtzConfigCam;

import javax.swing.*;
import java.awt.*;

public class PtzCamera extends JPanel {

    private PtzConfigCam camera;

    public PtzCamera(PtzConfigCam camera) {
        super(new GridLayout(3, 1, 5, 1));
        this.camera = camera;
        add(new JLabel("Name", SwingConstants.RIGHT));
        add(new JTextField(camera.name()));
        add(new JLabel("Host", SwingConstants.RIGHT));
        add(new JTextField(camera.host()));
        add(new JLabel("Play Mode", SwingConstants.RIGHT));
        var buttonGroup = new ButtonGroup();
        // JPanel buttonPanel = new JPanel(new GridLayout(1, 3));
        var buttonPanel = new JToolBar();
        var asc = new JCheckBox("ASC");
        var desc = new JCheckBox("DESC");
        var rand = new JCheckBox("RAND");
        buttonPanel.add(asc);
        buttonPanel.add(desc);
        buttonPanel.add(rand);
        buttonGroup.add(asc);
        buttonGroup.add(desc);
        buttonGroup.add(rand);
        add(buttonPanel);
    }

}
